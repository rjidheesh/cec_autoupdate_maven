/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.autoupdate;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author jideesh
 */
public class AutoUpdate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
        // TODO code application logic here
        try 
        {
            String url, version;
            File file = new File("url.txt");
            if(file.createNewFile())
            {
                System.out.println("New file created");
                JSONObject url1 = new JSONObject();
                url1.put("url","https://ubimedique.in/helpdesk/CEC");
                url1.put("version","CEC");
                url1.put("website","https://ubimedique.in");
                url1.put("subfolder", "/CEC/CEC_Jars/");
                FileWriter writer =  new FileWriter(file);
                writer.write(url1.toJSONString());
                writer.flush();
                writer.close();
                checkupdate();
                Runtime.getRuntime().exec("java -jar CEC.jar");
                
            }
            else
            {
                JSONParser parser = new JSONParser() ;
                try
                {
                    
                    
                    Path currentRelativePath = Paths.get("");
                    String s = currentRelativePath.toAbsolutePath().toString();
                    File dir = new File(s+"/Output");
                    File dest = new File(s);
                    if(dir.exists())
                    {
                        System.out.println("Updating...");
                        
                        copyDirectory(dir , dest);
                        
                        System.out.println("copied successfully");
                        
                        
                        FileUtils.deleteDirectory(dir);
                        
                        
                        Object obj = parser.parse(new FileReader("url.txt"));
                        JSONObject jsonObject =  (JSONObject) obj;
                        version = (String) jsonObject.get("version");
//                        System.out.println();
                        Runtime.getRuntime().exec("java -jar CEC.jar");
                        
                    }
                    else
                    {
                        Object obj = parser.parse(new FileReader("url.txt"));
                        JSONObject jsonObject =  (JSONObject) obj;
                        version = (String) jsonObject.get("version");
                        Runtime.getRuntime().exec("java -jar CEC.jar");
                        checkupdate();
                        
                    }
                    
                }catch (Exception e)
                {
                    System.out.println("Error in launching the app");
                }
                
                
                
            }
        } catch (IOException ex) 
        {
            Logger.getLogger(AutoUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private static void checkupdate() {
        // Checking update in server
        try
        {
            
            System.out.println("Started version checking");
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader("url.txt"));
            JSONObject jsonObject =  (JSONObject) obj;
            String version = (String) jsonObject.get("version");
            String website = (String) jsonObject.get("website");
            String subfolder = (String) jsonObject.get("subfolder");
//            String 
            System.out.println(website+subfolder);
            URL web = new URL(website+subfolder+"version.txt");
           
            InputStream in = web.openStream();
            int ch;
            StringBuilder sb = new StringBuilder();
            int i=0;
            while((ch = in.read()) != -1)
            {
                sb.append((char)ch);
                i++;
                
                
            }
            sb.setLength(sb.length()-1);
            
            String latestAppVersion = sb.toString();
            System.out.println("version found on the server :"+latestAppVersion);
//            version = version.replace("\n", "");
            System.out.println("Current Version: "+version);
            
            if(version.equals(latestAppVersion))
            {
                System.out.println("No updates found");
            }
            else
            {
                Path pathToFile = Paths.get("./dist.zip");
                System.out.println("Found new update");
                URL downloadurl = new URL(website+subfolder+latestAppVersion+".zip");
                System.out.println(downloadurl);
                try (InputStream input = downloadurl.openStream())
                {
                    Files.copy(input ,pathToFile, StandardCopyOption.REPLACE_EXISTING);
                    System.out.println("Downloaded update");
                    ZipFile sourcefile = new ZipFile("dist.zip");
                    String zipFilePath = "dist.zip";
                   
                    Path currentRelativePath = Paths.get("");
                    String s = currentRelativePath.toAbsolutePath().toString();
                    String destDir = s+"/output";
                    
                    
                    unzip(zipFilePath, destDir);
                    
                    JSONParser parser1 = new JSONParser();
                    File file = new File("url.txt");
                    Object obj1 = parser1.parse(new FileReader("url.txt"));
                    JSONObject jsonObject1 =  (JSONObject) obj1;
                    
                    
                    jsonObject1.put("version", latestAppVersion);
                    
                    FileWriter writer =  new FileWriter(file);
                    writer.write(jsonObject1.toJSONString());
                    writer.flush();
                    writer.close();
                    
                    System.out.println("Version info modified");
                    
                }
                
                
            }
            
            
        }
        catch (Exception ex)
        {
            Logger.getLogger(AutoUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void unzip(String zipFilePath, String destDir) {
        try(ZipFile file = new ZipFile(zipFilePath))
        {
            FileSystem fileSystem = FileSystems.getDefault();
            //Get file entries
            Enumeration<? extends ZipEntry> entries = file.entries();
             
            //We will unzip files in this folder
            String uncompressedDirectory = "Output/";
            Files.createDirectory(fileSystem.getPath(uncompressedDirectory));
             
            //Iterate over entries
            while (entries.hasMoreElements())
            {
                ZipEntry entry = entries.nextElement();
                //If directory then create a new directory in uncompressed folder
                if (entry.isDirectory())
                {
                    System.out.println("Creating Directory:" + uncompressedDirectory + entry.getName());
                    Files.createDirectories(fileSystem.getPath(uncompressedDirectory + entry.getName()));
                }
                //Else create the file
                else
                {
                    InputStream is = file.getInputStream(entry);
                    BufferedInputStream bis = new BufferedInputStream(is);
                    String uncompressedFileName = uncompressedDirectory + entry.getName();
                    Path uncompressedFilePath = fileSystem.getPath(uncompressedFileName);
                    Files.createFile(uncompressedFilePath);
                    FileOutputStream fileOutput = new FileOutputStream(uncompressedFileName);
                    while (bis.available() > 0)
                    {
                        fileOutput.write(bis.read());
                    }
                    fileOutput.close();
                    System.out.println("Written :" + entry.getName());
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void copyDirectory(File sourceLocation , File targetLocation) throws FileNotFoundException, IOException {
       

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }

            String[] children = sourceLocation.list();
            for (int i=0; i<children.length; i++) {
                copyDirectory(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
        
    }
    
}
